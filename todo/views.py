from todo.models import Todo
from rest_framework import viewsets
from todo.serializers import TodoSerializer


class TodoViewSet(viewsets.ModelViewSet):
    queryset = Todo.objects.all().order_by("tanggal", "jam")
    serializer_class = TodoSerializer
