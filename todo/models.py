from django.db import models

# Create your models here.
class Todo(models.Model):
    nama = models.CharField(max_length=128)
    tanggal = models.DateField()
    jam = models.TimeField()
    is_done = models.BooleanField(default=False)
